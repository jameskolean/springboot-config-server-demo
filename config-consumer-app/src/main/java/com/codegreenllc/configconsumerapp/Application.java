package com.codegreenllc.configconsumerapp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class Application {

	@RefreshScope
	@RestController
	class MessageRestController {

		@Value("${message:Hello default}")
		private String message;

		@Value("${native.message:Native default}")
		private String nativeMessage;

		@Value("${vault.message:Vault default}")
		private String vaultMessage;

		@RequestMapping("/message")
		String getMessage() {
			return String.format("Native property is '%s', Git property is '%s', and Vault property is '%s'",
					nativeMessage, message, vaultMessage);
		}
	}

	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
