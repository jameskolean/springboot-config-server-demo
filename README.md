#Vault from Hashicorp
Follow these steps to set up Vault for local testing
```
brew install vault
vault -autocomplete-install
vault server -dev
export VAULT_ADDR='http://127.0.0.1:8200'
vault status
vault kv put secrets/config-consumer-app vault.message="Hello from Vault"
vault kv get -format=json secret/config-consumer-app
```

# Running
```
mvn -pl config-server spring-boot:run -Dspring-boot.run.profiles=native,git,vault,local
mvn -pl config-consumer-app spring-boot:run -Dspring-boot.run.profiles=local
```
# View properties from server
```
curl http://localhost:8888/config-consumer-app/default
```
# Trigger client property refresh
```
curl localhost:8080/actuator/refresh -d {} -H "Content-Type: application/json"
```
# Dockerize
You may need to login in
`docker login`
or
`docker login registry.gitlab.com`

## Build, Publish, and Run
```
mvn clean && mvn --projects config-server,config-consumer-app package jib:build
docker run -p 8443:8443 -e "SPRING_PROFILES_ACTIVE=native,git,vault" -e "spring.cloud.config.server.vault.token=YOUR-TOKEN" -t "jameskolean/config-server:0.0.1-SNAPSHOT"
docker run -p 8080:8080 -t "jameskolean/config-consumer-app:0.0.1-SNAPSHOT"
```





